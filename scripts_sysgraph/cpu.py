#!/usr/bin/env python
#coding:utf-8

from datetime import datetime
import psutil

def CPU():
	cpu= {}
	cpu['utilizacao_total'] = psutil.cpu_percent(interval=1)
	cpu['livre'] = 100 - cpu['utilizacao_total']
	
	result = psutil.cpu_percent(interval=1,percpu=True)
	for i in range(len(result)):
		cpu['cpu[%d]' %(i)]= result[i]
	cpu['datetime'] = str(datetime.now())
	return cpu

